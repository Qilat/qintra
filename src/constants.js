const API_URL =
  process.env.NODE_ENV === 'development'
    ? 'http://localhost:8080'
    : 'https://api.qilat.fr';

export const API_URLS = {
  v1: {
    fiches: API_URL + '/v1/fiches',
  },
};
