import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.scss';
import Home from './pages/home/Home';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Sidebar from './components/sidebar/sidebar';
import Fiches from './pages/fiches/Fiches';
import StandardLayer from './components/layers/standard-layer/standard-layer';
import FicheViewer from './pages/ficheviewer/fiche-viewer';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  //<React.StrictMode>
  <BrowserRouter>
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/nav" element={<Sidebar />} />
      <Route
        path="/fiches/:name"
        element={
          <StandardLayer>
            <FicheViewer />
          </StandardLayer>
        }
      ></Route>
      <Route
        path="/fiches"
        element={
          <StandardLayer title={'Fiches Codex'}>
            <Fiches />
          </StandardLayer>
        }
      ></Route>
      <Route path="/*" element={<div>404</div>} />
    </Routes>
  </BrowserRouter>
  //</React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
