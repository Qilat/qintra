import React from 'react';
import './sidebar.scss';
import {
  FaBook,
  FaBookDead,
  FaHouseUser,
  FaThLarge,
  FaWineBottle,
} from 'react-icons/fa';
import { Link } from 'react-router-dom';

class Sidebar extends React.Component {
  render() {
    return (
      <div className={'sidebar ' + (this.props.collapsed ? 'collapsed' : '')}>
        <div className="sidebar-title">
          <div className="sidebar-collapsed-text">QIntra</div>
          <div className="icon" onClick={this.props.toggle}>
            <FaThLarge />
          </div>
        </div>

        <nav>
          <li>
            <Link to={'/'}>
              <div className="icon">
                <FaHouseUser />
              </div>
              <div className="sidebar-collapsed-text">Home</div>
            </Link>
          </li>
          <li>
            <Link to={'/fiches'}>
              <div className="icon">
                <FaBook />
              </div>
              <div className="sidebar-collapsed-text">Fiches</div>
            </Link>
          </li>
          <li>
            <Link to={'/nav'}>
              <div className="icon">
                <FaWineBottle />
              </div>
              <div className="sidebar-collapsed-text">Nav</div>
            </Link>
          </li>
          <li>
            <Link to={'/404'}>
              <div className="icon">
                <FaBookDead />
              </div>
              <div className="sidebar-collapsed-text">Dead link</div>
            </Link>
          </li>
        </nav>

        <div className={'sidebar-footer'}>
          <div className="sidebar-collapsed-text">Created by Qilat</div>
        </div>
      </div>
    );
  }
}

export default Sidebar;
