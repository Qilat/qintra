import React from 'react';
import './standard-layer.scss';
import Sidebar from '../../sidebar/sidebar';

class StandardLayer extends React.Component {
  constructor(props) {
    super(props);
    this.toggleSidebar = this.toggleSidebar.bind(this);
    this.state = {
      sidebarCollapsed: true,
    };
  }

  toggleSidebar() {
    this.setState({
      ...this.state,
      sidebarCollapsed: !this.state.sidebarCollapsed,
    });
  }

  render() {
    return (
      <div className="standard-layer">
        <Sidebar
          toggle={this.toggleSidebar}
          collapsed={this.state.sidebarCollapsed}
        ></Sidebar>
        <div className="page">
          <div className={'title' + (!this.props.title ? ' hidden' : '')}>
            {this.props.title}
          </div>
          <div className="content">{this.props.children}</div>
        </div>
      </div>
    );
  }
}

export default StandardLayer;
