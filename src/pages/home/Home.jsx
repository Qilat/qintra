import React from 'react';
import './Home.scss';
import StandardLayer from '../../components/layers/standard-layer/standard-layer';

class Home extends React.Component {
  render() {
    return <StandardLayer></StandardLayer>;
  }
}

export default Home;
