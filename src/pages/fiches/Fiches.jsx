import React from 'react';
import './Fiches.scss';
import axios from 'axios';
import { API_URLS } from '../../constants';
import { Link } from 'react-router-dom';

class Fiches extends React.Component {
  constructor(props) {
    super(props);

    this.sortFiles = this.sortFiles.bind(this);
    this.convertName = this.convertName.bind(this);
    this.convertSize = this.convertSize.bind(this);
    this.convertMTime = this.convertMTime.bind(this);
    this.filterFiles = this.filterFiles.bind(this);

    this.state = {
      files: [],
    };
  }

  sortFiles() {
    const collator = new Intl.Collator(undefined, {
      numeric: true,
      sensitivity: 'base',
    });

    let files = this.state.files.sort((a, b) =>
      collator.compare(a.name, b.name)
    );

    this.setState({
      ...this.state,
      files,
    });
  }

  convertName(name) {
    return name.substring(0, name.length - 4);
  }

  convertSize(size) {
    if (size > 1000) {
      size = Math.round(size / 1000);

      if (size > 1000) {
        size = Math.round(size / 1000);
        size += ' Mo';
      } else {
        size += ' Ko';
      }
    }
    return size;
  }

  convertMTime(mtime) {
    return (
      mtime[8] +
      '' +
      mtime[9] +
      '/' +
      mtime[5] +
      '' +
      mtime[6] +
      '/' +
      mtime[2] +
      '' +
      mtime[3]
    );
  }

  componentDidMount() {
    axios.get(API_URLS.v1.fiches).then((res) => {
      let files = res.data;
      console.dir(files);

      for (const file of files) {
        file.niceName = this.convertName(file.name);
        file.size = this.convertSize(file.size);
        file.mtime = this.convertMTime(file.mtime);
      }

      this.setState(
        {
          ...this.state,
          baseFiles: files,
          files,
        },
        () => this.sortFiles()
      );
    });
  }

  filterFiles(e) {
    const value = e.target.value;
    const files = [];
    for (const file of this.state.baseFiles) {
      if (file.name.toLowerCase().includes(value.toLowerCase()))
        files.push(file);
    }

    this.setState(
      {
        ...this.state,
        files,
      },
      () => this.sortFiles()
    );
  }

  render() {
    return (
      <table className="table" cellPadding="0" cellSpacing="0">
        <thead>
          <tr>
            <td colSpan="3">
              <input
                className={'research-bar'}
                type="text"
                placeholder={'Recherche...'}
                value={this.state.value}
                onChange={this.filterFiles}
              />
            </td>
          </tr>
          <tr>
            <td className={'header-title'}>Nom</td>
            <td className={'header-title'}>Taille</td>
            <td className={'header-title'}>Dernière modif.</td>
          </tr>
        </thead>
        <tbody>
          {this.state.files.map((file, i) => (
            <tr className={'file-row'} key={i}>
              <td className={'name'}>
                <Link to={'/fiches/' + file.name} key={file.name}>
                  {file.niceName}
                </Link>
              </td>
              <td className={'size'}>{file.size}</td>
              <td className={'mtime'}>{file.mtime}</td>
            </tr>
          ))}
        </tbody>
      </table>
    );
  }
}

export default Fiches;
