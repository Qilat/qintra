import React, { useEffect, useState } from 'react';
import './fiche-viewer.scss';
import axios from 'axios';
import { API_URLS } from '../../constants';
import { Document, Page } from 'react-pdf/dist/esm/entry.webpack5';
import { useParams } from 'react-router-dom';

function FicheViewer() {
  const [numPages, setNumPages] = useState(null);
  const [file, setFile] = useState(null);
  const { name } = useParams();

  useEffect(() => {
    axios
      .get(API_URLS.v1.fiches, {
        params: {
          name,
        },
        responseType: 'arraybuffer',
      })
      .then((res) => {
        console.dir(res);
        setFile(
          new File([res.data], name, {
            type: 'application/pdf',
          })
        );
      })
      .catch((e) => {
        console.error(e);
      });
  }, [name]);

  function onDocumentLoadSuccess({ numPages }) {
    setNumPages(numPages);
  }

  return (
    <div className={'container'}>
      <Document
        file={file}
        onLoadSuccess={onDocumentLoadSuccess}
        className={'document'}
        renderMode={'svg'}
      >
        {Array.apply(null, Array(numPages))
          .map((x, i) => i + 1)
          .map((page) => (
            <Page className={'pdf-page'} pageNumber={page} key={page} />
          ))}
      </Document>
    </div>
  );
}

export default FicheViewer;
